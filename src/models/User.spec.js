/*eslint-env mocha */
'use strict'
import _ from 'lodash'
import { expect } from 'chai'
import Db from '../../src/index'
// import mocha_generators from 'mocha-generators'

// mocha_generators.install()
// require('mocha-generators')()

let db = {}

describe('User', function () {
  this.timeout(1000*5)
  beforeEach(()=>{
    db = Db()
  })
  afterEach(()=>{
    db.close()
  })
  it('db has Model', function () {
    expect(db.models.User).to.be.function
  })
})
