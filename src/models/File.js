import Mongoose from 'mongoose'
import Storage from 'storage'
import Debug from 'debug'
import co from 'co'

const debug = Debug('db')
function Schema(){
  let schema = new Mongoose.Schema({
    title: {
      type: String
      , required: true
    }
    , img: {
      type: String
      , required: true
    }
    , tags: {
      type: [String]
    }
    , downloadUrl: {
      type: String
    }
    , filename: {
      type: String
      , required: true
    }
    , user: {
      type: Mongoose.Schema.Types.ObjectId
    }
  })
  schema.pre('remove', function(next, done){
    let ctx = this
    //delete shit from storage.
    co(function*(){
      if(ctx.filename)
      yield deleteFile('cadq', ctx.filename)
      if(ctx.img)
      yield deleteFile('cadimg', ctx.img)
      debug('#delete files')
    })
    .then(()=>{
      done()
    })
    .catch(err=>{
      debug(err)
      done(err)
    })
  })
  return schema
}
function deleteFile(bucket, filename){
  return new Promise((resolve, reject)=>{
    let storage = new Storage(bucket)
    let req = storage.del(filename)
    .on('response', res=>{
      if(res.statusCode!= 204) return reject(new Error(res.statusMessage))
      resolve()
    })
    .on('error', reject)
    .end()
  })
}
export default Schema