/*eslint-env mocha */
'use strict'
import _ from 'lodash'
import { expect } from 'chai'
import Db from '../../src/index'
import Debug from 'debug'
import request from 'superagent'
import Storage from 'storage'
import timer from 'co-timer'
const debug = Debug('test')

let imgUrl = 'https://s3.amazonaws.com/cadimg/55836ab2c6e096f4d5659a6d.jpg'
let fileUrl = 'https://s3.amazonaws.com/cadq/55836874b9421569d5675472.zip'
// import mocha_generators from 'mocha-generators'

// mocha_generators.install()
// require('mocha-generators')()

let db = {}
let storage = new Storage('cadq')
let imgStorage = new Storage('cadimg')
describe('File', function () {
  this.timeout(1000*5)
  beforeEach(()=>{
    db = Db()
  })
  afterEach(()=>{
    db.close()
  })
  it('db has Model', function () {
    expect(db.models.File).to.be.function
  })
  it.only('should delete files in storage on pre destroy', function* () {
    let file = new db.models.File({
      title: 'test_title'
    })
    let imgName = `${file._id}.jpg`
    let img = request.get(imgUrl)
    let header = {
      'Content-Type':'image/jpeg'
    }
    // debug(imgName)
    yield imgStorage.putStreamBuffer(img, imgName, header)

    let filename = `${file._id}.zip`
    let _file = request.get(fileUrl)
    header = {
      'Content-Type':'application/zip'
    }
    yield storage.putStreamBuffer(_file, filename, header)

    file.set('filename', filename)
    file.set('img', imgName)
    yield file.save()
    // debug(file.toJSON())
    let res = yield file.remove()
  })
})
