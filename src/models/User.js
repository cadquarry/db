let Mongoose = require('mongoose')

function Schema(){
  let schema = new Mongoose.Schema({
    _id: {
      type: Mongoose.Schema.Types.Mixed
      , required: '{PATH} is required.'
    }
    , password: {
      type: String
      , bcrypt: true
      , rounds: 8
      , required: true
    }
    , FName: {
      type: String
      , required: true
    }
    , LName: {
      type: String
      , required: true
    }
  })

  schema.plugin(require('mongoose-bcrypt'))

  return schema
}
export default Schema