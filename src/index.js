'use strict'
import Mongoose from 'mongoose'
import requireall from 'require-all'
import _ from 'lodash'
import path from 'path'
import User from './models/User'
import File from './models/File'

let connStr = 'mongodb://localhost:27017/cadquarry'
if(process.env.NODE_ENV=='production')
connStr = 'mongodb://chinnno15:Chuy081805@ds047632.mongolab.com:47632/cadquarry'

let models = {
  User: User
  , File: File
}

function clearModels(Mongoose){
  Mongoose.models = {}
}

class Connection {
  static CreateConnection(){
    let conn = Mongoose.createConnection(connStr)

    clearModels(conn)

    let modelKeys = _.keys(models)

    modelKeys.forEach((k)=>{
        let schema = models[k]
        conn.model(k, schema())
    })
    return conn
  }
}
export default Connection.CreateConnection

