ifeq (watch,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
	ifeq ($(RUN_ARGS),)
		RUN_ARGS :=*
	endif
  $(eval $(RUN_ARGS):;@:)
endif

test:
		@if find -name 'node_modules' -prune -o -type f -name '*.js' | xargs grep -q '^\s*debugger'; then \
		./node_modules/.bin/mocha -u bdd --debug-brk $(TESTS) --harmony --compilers js:babel/register; \
		else \
		./node_modules/.bin/mocha -u bdd $(TESTS) --harmony --compilers js:babel/register; \
		fi


watch:
	@./node_modules/.bin/mocha -w -u bdd --recursive --reporter spec test/ --require co-mocha --harmony-generators --compilers js:babel/register

.PHONY: test
